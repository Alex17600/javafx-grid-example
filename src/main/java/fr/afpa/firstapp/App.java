package fr.afpa.firstapp;


import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import javafx.event.*;

/**
 * Application de démonstration
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {       

        // Instanciation des composants graphique
        Label userNameLabel = new Label("Nom d'utilisateur :");
        Label passwordLabel = new Label("Mot de passe :");
        TextField userNameTextField = new TextField();
        PasswordField passwordField = new PasswordField();
        Label messageLabel = new Label();
        messageLabel.setPadding(new Insets(5, 10, 5, 10));
        Button connectionButton = new Button("Connexion");
        
        // Création d'une classe locale pour la gestion de l'évènement "click"
        EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String userName = userNameTextField.getText();
                String password = passwordField.getText();
                if (userName.isEmpty() || password.isEmpty()) {
                    messageLabel.setText("Veuillez saisir tous les champs.");
                    messageLabel.setStyle("-fx-text-fill: WHITE; -fx-background-color: RED");   
                } else {
                    messageLabel.setText("Connexion acceptée pour l'utilisateur " + userName);
                    messageLabel.setStyle("-fx-text-fill: WHITE; -fx-background-color: GREEN");   
                }
            }
        };

        connectionButton.addEventHandler(MouseEvent.MOUSE_CLICKED, eventHandler);

        // Instanciation d'un pane grille pour organisation des composants graphiques 
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        // ajout de "padding" pour l'écarter du bord
        gridPane.setPadding(new Insets(10, 10, 10, 10));

        gridPane.add(userNameLabel, 0, 0);
        GridPane.setVgrow(userNameLabel, Priority.ALWAYS);
        gridPane.add(userNameTextField, 1, 0);
        GridPane.setHgrow(userNameTextField, Priority.ALWAYS);

        gridPane.add(passwordLabel, 0, 1);
        GridPane.setVgrow(passwordLabel, Priority.ALWAYS);

        gridPane.add(passwordField, 1, 1);
        GridPane.setHgrow(passwordField, Priority.ALWAYS);
        // gridPane.setGridLinesVisible(true);

        // Ajout du message d'information
        messageLabel.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(messageLabel, 0, 2);
        GridPane.setColumnSpan(messageLabel, 2);

        // Ajout du bouton
        // avec gestion de la taille du button et du "span" sur plusieurs colonnes
        // On autorise le bouton à prendre toute la place disponible
        connectionButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        gridPane.add(connectionButton, 0, 3);
        GridPane.setColumnSpan(connectionButton, 2);
        
        // Intanciation d'un "pane" racine
        // permet de faire "coller" au bord ce qu'il contient
        AnchorPane root = new AnchorPane();
        // Ajout du "gridPane" à la liste des enfants de la racine
        root.getChildren().add(gridPane);
        // paramétrage du "pane" racine afin de faire coller la grille
        AnchorPane.setLeftAnchor(gridPane, 0.0);
        AnchorPane.setRightAnchor(gridPane,  0.0);
        AnchorPane.setTopAnchor(gridPane, 0.0);
        AnchorPane.setBottomAnchor(gridPane,  0.0);

        // Création de la scène à afficher (le contenu de la fenêtre)
        Scene scene = new Scene(root);
        // Ajout de la scène à la fenêtre
        stage.setScene(scene);
        // Affichage de la fenêtre
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    // Classe INTERNE de gestion d'évènement 
    // -> à titre d'exemple
    // public class ConnexionHandler implements EventHandler<MouseEvent> {
    //     @Override
    //     public void handle(MouseEvent event) {
    //         System.out.println("Bonjour !");
    //     }
    // }
}